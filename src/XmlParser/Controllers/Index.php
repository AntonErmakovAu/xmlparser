<?php
/**
 * Created by PhpStorm.
 * User: Anton Ermakov
 * Date: 07.12.2016
 */

namespace XmlParser\Controllers;

use \XmlParser\Models\ImgExtractor as ImgExtractorModel;
use \Swiftlet\Abstracts\Controller as ControllerAbstract;

/**
 * Main controller
 */
class Index extends ControllerAbstract
{
	/**
	 * Page title
	 * @var string
	 */
	protected $title = 'XML Parser';
    //public $performance;

	/**
	 * Default action
	 * @param $args array
	 */
	public function index(array $args = array())
	{
		$xmlParser = new ImgExtractorModel;
        $xmlParser->xmlSource = 'https://www.reddit.com/r/pics.xml';
        $this->view->imgFromXML = $xmlParser->getImgFromXML();
        $this->view->parsError = $xmlParser->parsError;
	}
}
