<?php
/**
 * Created by PhpStorm.
 * User: Anton Ermakov
 * Date: 07.12.2016
 */

namespace XmlParser\Listeners;

use \Swiftlet\Abstracts\Controller as ControllerAbstract;
use \Swiftlet\Abstracts\Listener as ListenerAbstract;
use \Swiftlet\Abstracts\View as ViewAbstract;

/*
 * Calculate the time for XML source parsing
 */
class ParserPerformance extends ListenerAbstract
{

    public function actionBefore(ControllerAbstract $controller, ViewAbstract $view)
    {
        $this->app->performance = -microtime(true);
    }

	public function actionAfter(ControllerAbstract $controller, ViewAbstract $view)
    {
        $this->app->performance += microtime(true);
        $view->statistics = 'XML parsing time: ' . round($this->app->performance,3) . ' secs.';
	}
}
