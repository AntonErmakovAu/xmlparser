<?php
/**
 * Created by PhpStorm.
 * User: Anton Ermakov
 * Date: 07.12.2016
 */

namespace XmlParser\Models;

use \Swiftlet\Abstracts\Model as ModelAbstract;

/**
 * Model for extracting and storing the last image from the feedback at http://www.reddit.com/r/pics.xml
 */
class ImgExtractor extends ModelAbstract
{

	public $parsError;
    public $imgHTML;
    public $xmlSource;


    public function getImgFromXML($source='') {

        if ($source) {
            $this->xmlSource = $source;
        }

        if ($this->xmlSource) {

            $xmlContent = @file_get_contents($this->xmlSource);

            if ($xmlContent) {
                // xpath request requires registered xml namespace. In order to simplify the task apply the next hack.
                $xmlContent = str_replace('xmlns=', 'ns=', $xmlContent);
                $xml = @simplexml_load_string($xmlContent);

                if ($xml) {
                    $res = $xml->xpath("(//content)[last()]");

                    // Inspite of manual use parentheses (//tag) in xpath() because of a strange behavior in different environment.
                    // Additional check was added to prevent possible script failure.
                    // Get last XMLElement if xpath() accidentally returns array.
                    $content=(count($res)>1)?$res[count($res)-1][0]:reset($res);

                    if (preg_match('/<img [^>]* \/>/i',$content,$img)) {
                        $this->imgHTML=$img[0];  
                    } else {
                        $this->parsError='Parser cannot find any image from the last feedback.';
                    }
                }
                else{
                    $this->parsError = 'Parser cannot process the file.';
                }
            } else {
                $this->parsError = "Fail to open {$this->xmlSource}.";
            }
        }
        else {
            $this->parsError = 'XML source must be defined!';
        }

        return $this->imgHTML;
    }

}